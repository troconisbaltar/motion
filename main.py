import os
import cv2
import asyncio
import numpy as np
from queue import deque
from time import sleep

from utils import *

os.chdir('/root/motion')

source = 0
cap = cv2.VideoCapture(source)

in_motion = False
previous_frame = None

frame_count = 0
idle_count = 0
detections_count = 0

frames_queue = deque(maxlen=1500)
FOURCC = cv2.VideoWriter_fourcc(*'XVID')

if not os.path.exists('./videos'):
    os.mkdir('./videos')

async def process_video(queue):
    print("Processing video")
    date = datetime.now().strftime('%m-%d-%Y_%H-%M-%S') 
    out = cv2.VideoWriter(f'./videos/{date}.avi', FOURCC, 30, (640, 480))

    while len(queue):
        frame = queue.pop()
        out.write(frame)

    out.release()

while True:
    ret, frame = cap.read()

    if ret:
        original_frame = frame.copy()
        prepared_frame = prepare_frame(frame)

        if previous_frame is None:
            previous_frame = prepared_frame
            continue

        if frame_count >= 100:
            previous_frame = prepared_frame
            frame_count = 0
        else:
            frame_count += 1

        contours = get_contours(previous_frame, prepared_frame)
        if not len(contours):
            idle_count += 1

        for contour in contours:
            if cv2.contourArea(contour) < 5000:
                idle_count += 1
            else:
                if not in_motion:
                    idle_count = 0
                    in_motion = True
                else:            
                    (x, y, w, h) = cv2.boundingRect(contour)
                    cv2.rectangle(img=frame, pt1=(x, y), pt2=(x + w, y + h), color=(0, 255, 0), thickness=2)

        #output = "Motion" if in_motion else "No Motion"
        #cv2.putText(frame, output, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 0), 1, cv2.LINE_AA)
        #cv2.imshow('Motion detector', frame)

        if in_motion:
            frames_queue.appendleft(original_frame)            

        if idle_count >= 150 and len(frames_queue) and not in_motion:
            print(f'Frames queue length before processing: {len(frames_queue)}')
            asyncio.run(process_video(frames_queue.copy()))
            frames_queue.clear()
            print(f'Frames queue length after proccesing: {len(frames_queue)}')

        if idle_count >= 150:
            in_motion = False
            idle_count = 0

        if cv2.waitKey(1) == 27:
            cap.release()
            break
    else:
        if source < 2:
            source += 1
        elif source == 2:
            source = 0
        sleep(1)
        cap.release()
        cap = cv2.VideoCapture(source)

cv2.destroyAllWindows()